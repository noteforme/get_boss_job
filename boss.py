#!/urs/bin/env python
# _*_ coding:utf-8 _*_
import requests
from lxml import etree
import csv,time

def get_boss_job(url):
    headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36'}
    response = requests.get(url,headers=headers)
    print(response.text)
    html = etree.HTML(response.text)

    job_list = html.xpath("//div[@class='job-list']/ul/li")

    # job_list=html.xpath("//*[@id='main']/div/div[3]/ul/li")


    for job in job_list:
        #提取job详情页地址
        job_url = job.xpath('.//div[@class="info-primary"]/h3/a/@href')[0]
        #提取job名称
        job_name = job.xpath('.//div[@class="job-title"]/text()')[0]
        #提取薪资范围
        job_price = job.xpath('.//span[@class="red"]/text()')[0]
        #提取工作地点
        job_info = job.xpath('.//div[@class="info-primary"]/p/text()')[0]
        #提取公司名
        company_name = job.xpath('.//div[@class="info-company"]//h3/a/text()')[0]
        #提取公司性质
        company_info = job.xpath('.//div[@class="info-company"]//p/text()')[0]
        #提取发布时间
        submit_data = job.xpath('.//div[@class="info-publis"]//p/text()')[0]


        # job_dict = dict(
        #     job_name = job_name,
        #     job_price = job_price
        # )
        # field = [job_name,job_price]
        with open('boss.csv','a',encoding='GBK') as f:
            temp = f'{job_name},{submit_data},{job_price},{job_info},{company_name},{company_info},{job_url}, \n'
            f.write(temp)
            print(temp)

if __name__ == '__main__':
    city_code = 'c101270100'
    job_keyword = 'java'
    for i in range(1,12):
        url = f'https://www.zhipin.com/{city_code}/?query={job_keyword}&page={i}&ka=page-{i}'
        print(url)
        get_boss_job(url)
        time.sleep(5)
        #备注：暂时没有添加异常处理，可以判断页面响应code后再去提取工作列表，以及具体字段也需要做异常